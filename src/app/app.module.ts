import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { YoutubeVideoPlayer } from '@ionic-native/youtube-video-player';
import {HttpModule} from '@angular/http'; 
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { LangPage } from '../pages/lang/lang';
import { LangPageModule } from '../pages/lang/lang.module';
import {SliderPage} from '../pages/slider/slider';
import {SliderPageModule} from '../pages/slider/slider.module';
import { IonicStorageModule } from '@ionic/storage';
import {MusicPage} from '../pages/music/music';
import {VideoPage} from '../pages/video/video';
import {MorePage} from '../pages/more/more';
import {ReadPage} from '../pages/read/read';
import {MusicPageModule} from '../pages/music/music.module';
import { WatchPage } from '../pages/watch/watch';
import {ContentPage} from '../pages/content/content';
import { GoogleMaps } from '@ionic-native/google-maps';
import { Geolocation } from '@ionic-native/geolocation';
import {VideoPageModule} from '../pages/video/video.module';
import {RecordingPageModule} from '../pages/recording/recording.module';
import {SubscribePageModule} from '../pages/subscribe/subscribe.module';
import { ProgramPage } from '../pages/program/program';
import { ProgramPageModule } from '../pages/program/program.module';
import { GoogleMaps } from '@ionic-native/google-maps';



@NgModule({
  declarations: [
    MyApp,
    HomePage
    ],

  
  
  imports: [
    BrowserModule,
    LangPageModule,
    SliderPageModule,
    ReadPageModule,
    VideoPageModule,
    MorePageModule,
    WatchPageModule,
    MusicPageModule,
    ProgramPageModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,LangPage,
    SliderPage,ProgramPage
  ],
  
  providers: [
    StatusBar,
    GoogleMaps,
    SplashScreen,
    GoogleMaps,
    IonicStorageModule,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
