import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import {LangPage} from '../lang/lang';
import { Storage } from '@ionic/storage';
import {Http} from '@angular/http';
import        'rxjs/add/operator/map';
import {SliderPage} from '../slider/slider';
import {MorePage} from '../more/more';
import {ReadPage} from '../read/read';
import {VideoPage} from '../video/video';
import {ContentPage} from '../content/content';
import {MusicPage} from '../music/music';
import { YoutubeVideoPlayer } from '@ionic-native/youtube-video-player';
import { WatchPage } from '../watch/watch';
import { ProgramPage } from '../program/program';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
 

  tab1:any;
  tab2:any;
  tab3:any;
  tab4:any;
  tab5:any;

  lang:any;
  rootPage:any;

 

  constructor(public navCtrl: NavController, private storage :Storage,public http :Http) {
    console.log("hi");
   
    this.tab1 = ContentPage;
    this.tab2 = WatchPage;
    this.tab3 =  MusicPage;
    this.tab4 = ReadPage;
    this.tab5 =  ProgramPage;

   

  	storage.get('lang').then((val) => {
    console.log('Your lang', val);

    this.lang = val;
    console.log(this.lang);
    if(this.lang == 'eng' || this.lang == 'tel'){
    	// this.navCtrl.setRoot(ContentPage);
    }
  });
  }
     ionViewDidLoad() {
  }


  

   remove(){
  	console.log(this.lang);
  	this.storage.remove('lang');
  	this.navCtrl.setRoot('LangPage');
  }

}
