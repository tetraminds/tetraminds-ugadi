import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {HomePage} from '../home/home';
import {LangPage} from '../lang/lang';
import {Storage} from '@ionic/storage';


/**
 * Generated class for the SliderPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-slider',
  templateUrl: 'slider.html',
})
export class SliderPage {
	lang:any;
	rootPage:any;

  imageData=[{image :"../assets/imgs/satyam_img.jpg"},{image :"../assets/imgs/satyam_img.jpg"},{image :"../assets/imgs/satyam_img.jpg"}]
  textData=[{name:"Events"},{name:"Articals"},{name:"Videos"}]
  constructor(public navCtrl: NavController, public navParams: NavParams,private storage :Storage) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SliderPage');
  }
  // goToHomepage() {
  // 	this.storage.get('lang').then((val) => {
  //   console.log('Your lang', val);

  //   this.lang = val;
  //   console.log(this.lang);
  //   if(this.lang == 'eng' || this.lang == 'tel'){
  //     this.navCtrl.setRoot(HomePage);
  //  }
  // });


}
