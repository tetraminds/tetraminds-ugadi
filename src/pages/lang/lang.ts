import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { SplashScreen } from '@ionic-native/splash-screen';
import {  HomePage } from '../home/home';
import {SliderPage} from '../slider/slider';
import { Storage } from '@ionic/storage';


@IonicPage()
@Component({
  selector: 'page-lang',
  templateUrl: 'lang.html',
})
export class LangPage {

	splash=true;

  constructor(public navCtrl: NavController, public navParams: NavParams, private splashScreen:SplashScreen,private storage :Storage) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LangPage');
    setTimeout(()=>{
    	 this.splash=false;
       },4000);
  }

  // ngOnInit() {
  //   this.splashScreen.show();
  	
  // }

  //  language(val){
  //   console.log(val);
  //   this.storage.set('lang',val);
  //   this.navCtrl.push(SliderPage);
  // }

}
