import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ContentPage } from './content';
// import {VideoPipe} from '../../pipes/video/video';
import { PipesModule } from '../../pipes/pipes.module';
@NgModule({
  declarations: [
    ContentPage,
    // VideoPipe
  ],
   // exports: [VideoPipe],
  imports: [
  	PipesModule,
    IonicPageModule.forChild(ContentPage),
  ],
})
export class ContentPageModule {}
