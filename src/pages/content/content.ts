import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { YoutubeVideoPlayer } from '@ionic-native/youtube-video-player';
import { VideoPipe } from '../../pipes/video/video';
import { PipesModule } from '../../pipes/pipes.module';
import { Storage } from '@ionic/storage';
import {Http} from '@angular/http';
import        'rxjs/add/operator/map';
import {SliderPage} from '../slider/slider';
import { SafeResourceUrl, DomSanitizer } from '@angular/platform-browser';


@IonicPage()
@Component({
  selector: 'page-content',
  templateUrl: 'content.html',
})
export class ContentPage {
	 channelId:string='UCULLmfWqhMOeiWEV6hkMSCg';
    maxRes:string='8';
    googleToken:string=' AIzaSyC1Q9V_RvWZ99Bzkf44eZoYhjseK_UINP4';
    search:string='Video';
    posts:any=[];
    postData:any;

     vidId:any;
     result:any;
	lang:any;
	rootPage:any;
  
  img =  "../assets/imgs/video-icon2.png";
  imageData= [{ image: "../assets/imgs/video-icon2.png" },{ image: "../assets/imgs/video-icon2.png" },{ image: "../assets/imgs/video-icon2.png" },{ image: "../assets/imgs/video-icon2.png" },{ image: "../assets/imgs/video-icon2.png" }]

  constructor(public navCtrl: NavController, public navParams: NavParams,public http:Http, private sanitizer:DomSanitizer) {
  	
  	let url="https://www.googleapis.com/youtube/v3/search?part=id,snippet&channelId="
    +this.channelId+"&q="+this.search+"&type=video&order=date&maxResults="+this.maxRes+"&key="+this.googleToken;
    
    this.http.get(url).map(res=>res.json()).subscribe(data=>{
      // this.postData = data.json();
      if(data!=undefined && data!=null 
      && data.items!=undefined && data.items!=null) {
      console.log(data.items);
      this.posts=this.posts.concat(data.items);
      console.log(this.posts);
      console.log('hiii');
      console.log(this.posts[0].id.videoId);
      for(let i=this.posts;i<this.posts.length;i++) {
      	console.log('jkmkks');
      	console.log(this.posts[i])
          this.vidId=this.posts[i].id.videoId;
          console.log(this.vidId);
      }
      }
    });
    this.getSafeUrl(url);
  }


  getSafeUrl(url){
      this.result=this.sanitizer.bypassSecurityTrustResourceUrl(url);
      console.log(this.result);

    }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ContentPage');
  }

}
